# -*- coding: utf-8 -*-
# Copyright 2019 Sergio Corato <https://github.com/sergiocorato>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

from openerp import models, api, exceptions, _, fields


class ProductAttribute(models.Model):
    _inherit = "product.attribute"

    customs_info = fields.Char('Customs info')


class ProductTemplate(models.Model):
    _inherit = "product.template"

    customs_info_ids = fields.Many2many(
        comodel_name='customs.info',
        relation='product_template_customs_info_rel',
        column1='product_tmpl_id',
        column2='customs_info_id',
        string='Customs info'
    )
