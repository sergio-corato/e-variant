# -*- coding: utf-8 -*-
# Copyright 2019 Sergio Corato <https://github.com/sergiocorato>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

from openerp import fields, models, api, _, exceptions


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    report_customs_info = fields.Boolean('Add customs info in report')
