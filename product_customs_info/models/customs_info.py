# -*- coding: utf-8 -*-
# Copyright 2019 Sergio Corato <https://github.com/sergiocorato>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

from openerp import fields, models


class CustomsInfo(models.Model):
    _name = "customs.info"

    name = fields.Char('Customs info')
    description = fields.Text()
    print_tsca = fields.Boolean()
    print_lacey = fields.Boolean()
