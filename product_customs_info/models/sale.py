# -*- coding: utf-8 -*-
# Copyright 2020 Sergio Corato <https://github.com/sergiocorato>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

from openerp import fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    report_customs_info = fields.Boolean('Add customs info in report')
