# -*- coding: utf-8 -*-
# Copyright 2019 Sergio Corato <https://github.com/sergiocorato>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).
{
    'name': 'Add fields for exports',
    'version': '8.0.1.0.0',
    'category': 'other',
    'author': 'Sergio Corato',
    'description': 'Add fields for exports.',
    'website': 'https://efatto.it',
    'license': 'LGPL-3',
    'depends': [
        'account',
        'product_code_builder_parent',
        'sale',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/account_invoice.xml',
        'views/customs_info.xml',
        'views/product.xml',
        'views/sale_order.xml',
    ],
    'installable': True
}