# -*- coding: utf-8 -*-
# Copyright 2020 Sergio Corato <https://github.com/sergiocorato>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from openerp import fields, models


class CountryGroup(models.Model):
    _inherit = 'res.country.group'

    company_note = fields.Char(company_dependent=True)
