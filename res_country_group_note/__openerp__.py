# -*- coding: utf-8 -*-
# Copyright 2020 Sergio Corato <https://github.com/sergiocorato>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    'name': 'Country Group Note',
    'version': '8.0.1.0.0',
    'category': 'other',
    'author': 'Sergio Corato - Efatto.it',
    'website': 'https://efatto.it',
    'description': 'Country Group Note',
    'license': 'AGPL-3',
    'depends': [
        'base',
    ],
    'data': [
        'views/res_country_group.xml',
    ],
    'installable': True
}
