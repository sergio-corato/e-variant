# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api, exceptions, _


class SaleOrderLine(models.Model):
    _inherit = ["sale.order.line", "product.configurator"]
    _name = "sale.order.line"

    @api.multi
    @api.onchange('product_tmpl_id')
    def onchange_product_template_id(self):
        self.ensure_one()
        # if product category has no_discount boolean, remove discount
        if self.product_tmpl_id.categ_id.no_discount:
            self.discount = 0.0
            self.complex_discount = False
        else:
            self.discount = self._context.get('default_discount', 0.0)
            self.complex_discount = self._context.get(
                'default_complex_discount', False)
