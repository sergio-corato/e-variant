# -*- coding: utf-8 -*-

import logging
import math
from openerp import models, api

_logger = logging.getLogger(__name__)


class WizardUpdateAttributePrice(models.TransientModel):
    _name = "wizard.update.attribute.price"
    _description = "Update Product Attribute Prices"

    @api.multi
    def update_attribute_prices(self):
        product_template_obj = self.env['product.template']
        product_ctg_obj = self.env['product.category']
        ir_parameter_obj = self.env['ir.config_parameter']

        product_excluded = []
        product_excluded_domain_string = ir_parameter_obj.get_param(
            'report.prices.variation.excluded.product.domain',
            default=[])
        product_excluded_domain = eval(product_excluded_domain_string)
        if isinstance(product_excluded_domain, list):
            product_excluded = product_template_obj.search(
                product_excluded_domain)

        excluded_product_ctg = []
        product_ctg_excluded_domain_string = ir_parameter_obj.get_param(
            'report.prices.variation.excluded.product.ctg.domain',
            default=[])
        product_ctg_excluded_domain = eval(product_ctg_excluded_domain_string)
        if isinstance(product_ctg_excluded_domain, list):
            excluded_product_ctg = product_ctg_obj.search(
                product_ctg_excluded_domain)

        # get a price variation from parameter, put 3.5 to get 3.5%
        variation = ir_parameter_obj.get_param(
            'report.prices.variation', default=False)

        if variation:
            try:
                variation = float(variation.replace(',', '.'))
            except ValueError as e:
                raise Exception('Price variation is not a number!')

            template_ids = product_template_obj.search([
                # ('attribute_line_ids', '!=', False),
                ('id', 'not in', product_excluded.ids),
                ('categ_id', 'not in', excluded_product_ctg.ids)
            ])
            if variation != 0.0:
                i = 0
                for template in template_ids:
                    i += 1
                    if template.attribute_line_ids:
                        found = False
                        for attribute_line in template.attribute_line_ids:
                            if attribute_line.price_extra != 0:
                                new_price = math.ceil(
                                    round(
                                        attribute_line.price_extra * (
                                            1 + variation / 100.0), 2)
                                )
                                _logger.info(
                                    '#%d/%d Updating prices for product id ;%s; %s -'
                                    ' code: '
                                    '%s - attribute ;%s; %s from price ;%d; to price'
                                    ' ;%d' % (
                                        i, len(template_ids), template.id,
                                        template.name,
                                        template.prefix_code,
                                        attribute_line.attribute_id.id,
                                        attribute_line.attribute_id.name,
                                        attribute_line.price_extra,
                                        new_price
                                    ))
                                attribute_line.price_extra = new_price
                                found = True
                        if not found:
                            if template.list_price != 0:
                                # update list_price - this template has price fixed- not
                                # computed from variants
                                new_price = math.ceil(
                                    round(
                                        template.list_price *
                                        (1 + variation / 100.0), 2)
                                )
                                _logger.info(
                                    '#%d/%d Updating list_price for product id ;%s;'
                                    ' %s - code: '
                                    '%s -;; from price ;%d; to price ;%d' % (
                                        i, len(template_ids), template.id,
                                        template.name,
                                        template.prefix_code,
                                        template.list_price,
                                        new_price
                                    ))
                                template.list_price = new_price
                    else:
                        if template.list_price != 0 and (
                            template.prefix_code
                            and 'RE' not in template.prefix_code.upper()) \
                            and (template.name and 'REFILL' not in template.name.upper()
                        ):
                            new_price = round(
                                template.list_price * (1 + variation / 100.0) + 0.5, 0)
                            _logger.info(
                                '#%d/%d Updating list_price for product id ;%s;'
                                ' %s - code: '
                                '%s -;; from price ;%d; to price ;%d' % (
                                    i, len(template_ids), template.id, template.name,
                                    template.prefix_code,
                                    template.list_price,
                                    new_price
                                ))
                            template.list_price = new_price

                return {
                    'name': "Updated Product Templates",
                    'view_mode': 'tree,form',
                    'res_model': 'product.template',
                    'type': 'ir.actions.act_window',
                    'domain': [('id', 'in', template_ids.ids)],
                }
