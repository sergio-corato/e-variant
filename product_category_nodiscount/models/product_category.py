# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import api, models, fields


class ProductCategory(models.Model):
    _inherit = "product.category"

    no_discount = fields.Boolean(help='This category never have discount')
