# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import api, models, fields


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    @api.cr_uid_ids_context
    def product_id_change(self, cr, uid, ids, pricelist, product, qty=0,
                          uom=False, qty_uos=0, uos=False, name='',
                          partner_id=False,
                          lang=False, update_tax=True, date_order=False,
                          packaging=False, fiscal_position=False, flag=False,
                          context=None):
        res = super(SaleOrderLine, self).product_id_change(
            cr, uid, ids, pricelist, product, qty=qty, uom=uom,
            qty_uos=qty_uos, uos=uos, name=name, partner_id=partner_id,
            lang=lang, update_tax=update_tax, date_order=date_order,
            packaging=packaging, fiscal_position=fiscal_position,
            flag=flag, context=context)
        if not product:
            return res
        if context is None:
            context = {}
        product_obj = self.pool['product.product']
        product_id = product_obj.browse(cr, uid, product, context)
        # if product category has no_discount boolean, remove discount
        if product_id.categ_id.no_discount:
            res['value']['discount'] = 0.0
            res['value']['complex_discount'] = False
        else:
            res['value']['discount'] = context.get('default_discount', 0.0)
            res['value']['complex_discount'] = context.get(
                'default_complex_discount', False)
        return res
