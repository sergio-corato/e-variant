# -*- coding: utf-8 -*-
#
#    Copyright (C) 2017 Sergio Corato
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

{
    'name': 'Products name search always on ilike',
    'version': '8.0.1.0.0',
    'category': 'Account',
    'license': 'AGPL-3',
    'description': """In the normal behaviour, searching product variants, if
    it is found a variant with exactly the name searched, the result show 
    only that variant. This module change this behaviour showing always all
    variant wich name is ilike the term searched.""",
    'author': "Sergio Corato",
    'website': 'http://www.efatto.it',
    'depends': [
        'product',
    ],
    'data': [
    ],
    'installable': True,
}
