# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api, _, exceptions


class SaleOrder(models.Model):
    _inherit = "sale.order"

    @api.multi
    def write(self, vals):
        if self._context.get('params', False):
            if self._context['params'].get('action', False) == \
                    self.env['ir.model.data'].get_object_reference(
                        'sale_order_mobile_variant',
                        'action_sale_order_mobile_variant')[1]:
                delivery_line_id = self.order_line.filtered(
                    lambda x: x.is_delivery)
                if 'carrier_id' not in vals:
                    if not delivery_line_id:
                        vals['carrier_id'] = self.env.user.company_id. \
                            default_delivery_carrier_id.id
        return super(SaleOrder, self).write(vals)