# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api


class AccountFiscalPosition(models.Model):
    _inherit = "account.fiscal.position"

    is_document_required = fields.Boolean(string='Document is required')


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    is_document_required = fields.Boolean(string='Document is required')
    required_document = fields.Binary(string='File required document')
    mrn_reference = fields.Char('MRN reference')
    rd_fname = fields.Char(string='Filename required document')

    @api.onchange('fiscal_position')
    def onchange_fiscal_position_required_document(self):
        if self.fiscal_position.is_document_required:
            self.is_document_required = True
        else:
            self.is_document_required = False
