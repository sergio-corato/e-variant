# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp.report import report_sxw
from openerp import fields
from openerp.exceptions import Warning as UserError
import re
try:
    import qrcode
except ImportError:
    qrcode = None
import StringIO


class Parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'labels': self._get_labels,
            'qr': self._qr,
        })
        self.cache = {}

    def _qr(self, text):
        text = text.encode('utf-8')
        qr_code = qrcode.QRCode(version=None, box_size=4, border=1)
        qr_code.add_data(text)
        qr_code.make(fit=True)
        qr_img = qr_code.make_image()
        draw = qr_img._img.convert("RGB")

        encoding = 'base64'
        code_stream = StringIO.StringIO()
        draw.save(code_stream, 'PNG')
        value = code_stream.getvalue().encode(encoding)
        code_stream.close()
        return value

    def _get_price(self, product_template_id, partner_id,
                   excluded_product_ctg_ids, product_excluded_ids,
                   attr_id=False):
        if not partner_id:
            raise Exception('No sale partner default configured in company!')
        price = False
        price_extra = 0.0
        pricelist_id = partner_id.property_product_pricelist
        attribute_line_obj = self.pool['product.attribute.line']
        if not pricelist_id:
            raise Exception('No pricelist configured in sale partner default!')
        if pricelist_id and product_template_id:
            if attr_id:
                attribute_line_ids = attribute_line_obj.search(
                    self.cr, self.uid, [
                    ('product_tmpl_id', '=', product_template_id.id),
                    ('attribute_id', '=', attr_id.id)
                ])
                if attribute_line_ids:
                    attribute_line_id = attribute_line_obj.browse(
                        self.cr, self.uid, attribute_line_ids[0]
                    )
                    price_extra = attribute_line_id.price_extra if\
                        attribute_line_id else 0.0
                    price = pricelist_id.with_context({
                        'uom': product_template_id.uom_id.id,
                        'date': fields.Date.today(),
                        'price_extra': price_extra,
                    }).template_price_get(
                        product_template_id.id, 1.0, partner_id.id)[pricelist_id.id]
            else:
                price = pricelist_id.with_context({
                    'uom': product_template_id.uom_id.id,
                    'date': fields.Date.today(),
                    'price_extra': price_extra,
                }).template_price_get(
                    product_template_id.id, 1.0, partner_id.id)[
                    pricelist_id.id]

        # get a price variation from parameter, put 3.5 to get 3.5%
        variation = self.pool['ir.config_parameter'].get_param(
            self.cr, self.uid, 'report.prices.variation',
            default=False)

        if variation and variation != 0 and product_template_id.categ_id.id not in \
                excluded_product_ctg_ids and price != 0.0 and \
                product_template_id.id not in product_excluded_ids:
            try:
                variation = float(variation.replace(',', '.'))
            except ValueError as e:
                raise UserError('Price variation is not a number!')
            if variation != 0.0:
                price = round(price * (1 + variation / 100.0) + 0.5, 0)

        return price

    def _encode_price(self, product_template_id, partner_id,
                      excluded_product_ctg_ids, product_excluded_ids,
                      attr_id=False):
        price = self._get_price(
            product_template_id, partner_id, excluded_product_ctg_ids,
            product_excluded_ids, attr_id=attr_id)
        if attr_id:
            price_text = ('010' if (attr_id.name == 'CAT. A' or
                                    attr_id.name == 'CAT. PA') else
                          '020' if attr_id.name == 'CAT. B' else
                          '030' if attr_id.name == 'CAT. C' else
                          '040' if attr_id.name == 'CAT. D' else
                          '050' if attr_id.name == 'CAT. E' else
                          '060' if attr_id.name == 'CAT. F' else
                          '070' if attr_id.name == 'CAT. G' else
                          '080' if attr_id.name == 'CAT. H' else ''
                          ) + str(int(price)) + '/00'
        else:
            price_text = '010' + str(int(price)) + '/00'
        return price_text

    def _get_labels(self, group_length=0):
        group_label = {}
        labels = []
        i = 0
        product_template_obj = self.pool['product.template']
        product_ctg_obj = self.pool['product.category']

        product_excluded_ids = []
        product_excluded_domain_string = self.pool['ir.config_parameter'].get_param(
            self.cr, self.uid, 'report.prices.variation.excluded.product.domain',
            default=[])
        product_excluded_domain = eval(product_excluded_domain_string)
        if isinstance(product_excluded_domain, list):
            product_excluded_ids = product_template_obj.search(
                self.cr, self.uid, product_excluded_domain)
        # ['|', '|', '|', ('prefix_code', 'ilike', 'ETRO'),
        # ('prefix_code', 'ilike', '.FR'), ('prefix_code', 'ilike', '.SP'),
        # ('name', 'ilike', 'SPECIAL')]

        excluded_product_ctg_ids = []
        product_ctg_excluded_domain_string = self.pool['ir.config_parameter'].get_param(
            self.cr, self.uid, 'report.prices.variation.excluded.product.ctg.domain',
            default=[])
        product_ctg_excluded_domain = eval(product_ctg_excluded_domain_string)
        if isinstance(product_ctg_excluded_domain, list):
            excluded_product_ctg_ids = product_ctg_obj.search(
                self.cr, self.uid, product_ctg_excluded_domain)
        # [('name', 'ilike', '2020')]

        if 'product.product.labels.order' in self.name:
            for order in self.pool['sale.order'].browse(
                    self.cr, self.uid, self.ids):
                for line in order.order_line.filtered(
                        lambda z: not (
                            z.product_id.is_pack or
                            z.product_id.service_type in [
                                'other', 'transport', 'contribution',
                                'discount']
                        ) and z.product_id):
                    for uom in range(0, int(line.product_uom_qty), 1):
                        group_label.update({
                            i: {
                                'code': line.product_id.default_code and
                                line.product_id.default_code or '',
                                'name': line.product_id.product_tmpl_id.name and
                                line.product_id.product_tmpl_id.name[:32] or '',
                                'leather': self._get_prod_leather(
                                    line.product_id),
                                'color': self._get_prod_stitching(
                                    line.product_id),
                            }
                        })
                        i += 1
                        if i == group_length:
                            labels += [group_label]
                            group_label = {}
                            i = 0
                if 0 < i < group_length:
                    for f in range(i, group_length, 1):
                        group_label.update({
                            f: {
                                'code': '',
                                'name': '',
                                'leather': '',
                                'color': '',
                            }
                        })
                    labels += [group_label]
        elif 'product.template.labels' in self.name:
            product_templates = []
            if self.name in ['product.template.labels',
                             'product.template.labels.pdf',
                             'product.template.labels.bh']:
                company_id = self.localcontext['company']
                partner_id = company_id.partner_id
                product_templates = product_template_obj.browse(
                    self.cr, self.uid, self.ids)
            elif self.name == 'product.template.labels.order':
                for order in self.pool['sale.order'].browse(
                        self.cr, self.uid, self.ids):
                    partner_id = order.partner_id
                    product_templates = order.order_line.filtered(
                        lambda y: not (
                            y.product_tmpl_id.is_pack or
                            y.product_tmpl_id.service_type in [
                                'other', 'transport', 'contribution',
                                'discount']
                        ) and y.product_tmpl_id).mapped('product_tmpl_id')
            for product in product_templates:
                attribute_prices = {}
                for attribute_line in product.attribute_line_ids:
                    if attribute_line.price_extra:
                        attribute_prices.update({
                            'VEN' if attribute_line.attribute_id.name.strip() ==
                            'VENEZIA PATTERN' else
                            'WIE' if attribute_line.attribute_id.name.strip() ==
                            'VIENNA PATTERN' else
                            'SUG' if attribute_line.attribute_id.name.strip() ==
                            'SUGAR PATTERN' else
                            'PRO' if attribute_line.attribute_id.name.strip() ==
                            'PROMENADE PATTERN' else
                            'TC' if attribute_line.attribute_id.name.strip() ==
                            'TECNOCUOIO' else
                            'SL' if attribute_line.attribute_id.name.strip() ==
                            'SADDLE LEATHER' else
                            'A' if attribute_line.attribute_id.name.strip() ==
                            'CAT. PA' else
                            'WHEELS' if 'WHEELS'
                            in attribute_line.attribute_id.name else
                            attribute_line.attribute_id.name.replace(
                                'CAT. ', '')[:4]:
                            self._encode_price(
                                product, partner_id, excluded_product_ctg_ids,
                                product_excluded_ids,
                                attribute_line.attribute_id)
                        })
                materials = {}
                base_code = False
                if product.prefix_code:
                    # search product wich starts with prefix code
                    # and make a dict of the part after prefix code
                    base_code = re.split('[A-Za-z]*$', product.prefix_code)[0]
                    template_child_ids = product_template_obj.search(
                        self.cr, self.uid, [
                            ('prefix_code', 'ilike', base_code)])
                    if template_child_ids:
                        for template in product_template_obj.browse(
                                self.cr, self.uid, template_child_ids):
                            if template.prefix_code and template.prefix_code[
                                    :len(base_code)] == base_code and \
                                    template.prefix_code[
                                    len(base_code):].upper() in \
                                    ['WENGE', 'WALNUT', 'NATURAL', 'BRONZE',
                                     'CHROME', 'BRASS', 'LC05', 'LC42', 'LC37',
                                     'LC21', 'BZ', 'LP', 'MC', 'GOLD']:
                                materials.update({
                                    template.prefix_code.replace(
                                        base_code, ''):
                                    template.prefix_code.replace(
                                        base_code, '')
                                })
                if product.list_price:
                    attribute_prices.update({
                        '- A': self._encode_price(
                            product, partner_id, excluded_product_ctg_ids,
                            product_excluded_ids
                        )
                    })
                group_label.update({
                    i: {
                        'default_code': base_code and base_code or
                        product.prefix_code and
                        product.prefix_code or
                        product.name and
                        product.name or False,
                        'name': product.name and
                        product.name[:50] or '',
                        'prices': attribute_prices,
                        'materials': materials,
                    }
                })
                i += 1
                if i == group_length:
                    labels += [group_label]
                    group_label = {}
                    i = 0
            if 0 < i < group_length:
                for f in range(i, group_length, 1):
                    group_label.update({
                        f: {
                            'default_code': '',
                            'name': '',
                            'prices': {},
                            'materials': {},
                        }
                    })
                labels += [group_label]
        elif self.name == 'product.template.list.price' or \
                self.name == 'product.template.list.price.xls' or \
                self.name == 'product.template.list.price.parmentier':
            company_id = self.localcontext['company']
            partner_id = company_id.partner_id
            children = []
            for product in product_template_obj.browse(
                    self.cr, self.uid, self.ids):
                if product in children or 'SPECIAL' in product.name or \
                        product.prefix_code and '.SP' in product.prefix_code.upper():
                    continue
                attribute_prices = {}
                for attribute_line in product.attribute_line_ids:
                    if attribute_line.attribute_id.name != 'STITCHING':
                        attribute_prices.update({
                            'SL' if attribute_line.attribute_id.name ==
                            'SADDLE LEATHER' else
                            'CAT. A' if attribute_line.attribute_id.name ==
                            'CAT. PA' else
                            attribute_line.attribute_id.name: {
                                'price': self._get_price(
                                    product, partner_id,
                                    excluded_product_ctg_ids,
                                    product_excluded_ids,
                                    attribute_line.attribute_id,
                                    ),
                                'children': (
                                    'STANDARD LEATHERS ' if attribute_line.
                                    attribute_id.name == 'CAT. A' else
                                    '' if attribute_line.attribute_id.name ==
                                    'CAT. B' else
                                    '' if attribute_line.attribute_id.name ==
                                    'CAT. C' else
                                    'EXOTIC LEATHERS ' if attribute_line.
                                    attribute_id.name == 'CAT. D' else ''
                                ) + ' - '.join(
                                    [x.code for x in attribute_line.
                                        attribute_id.child_ids.filtered(
                                            lambda z: len(z.code) == 1)]
                                )
                            }
                        })
                materials = []
                base_code = False
                if product.prefix_code:
                    # search product wich starts with prefix code
                    # and make a dict of the part after prefix code
                    base_code = re.split('[A-Za-z]*$', product.prefix_code)[0]
                    template_child_ids = product_template_obj.search(
                        self.cr, self.uid, [
                            ('prefix_code', 'ilike', base_code)])
                    if template_child_ids:
                        for template in product_template_obj.browse(
                                self.cr, self.uid, template_child_ids):
                            if template.prefix_code and template.prefix_code[
                                    :len(base_code)] == base_code and \
                                    template.prefix_code[
                                    len(base_code):].upper() in \
                                    ['WENGE', 'WALNUT', 'NATURAL', 'BRONZE',
                                     'CHROME', 'BRASS', 'LC05', 'LC42', 'LC37',
                                     'LC21', 'BZ', 'LP', 'MC', 'GOLD']:
                                materials.append(
                                    template.prefix_code.replace(
                                        base_code, ''))
                                children += template

                if product.list_price:
                    attribute_prices.update({
                        '- A': {
                            'price': self._get_price(
                                product, partner_id, excluded_product_ctg_ids,
                                product_excluded_ids),
                            'children': ''}
                    })
                labels += [{
                    i: {
                        'default_code': base_code and base_code or
                        product.prefix_code and
                        product.prefix_code or
                        product.name and
                        product.name or False,
                        'name': product.name and
                        product.name.upper().replace('MICRO', '').replace(
                            'MINI', '').replace('SMALL', '').replace(
                            'MEDIUM', '').replace('EXTRA LARGE', '').replace(
                            'LARGE', '') or '',
                        'size': 'MICRO' if 'MICRO' in product.name.upper()
                        else 'MINI' if 'MINI' in product.name.upper()
                        else 'SMALL' if 'SMALL' in product.name.upper()
                        else 'MEDIUM' if 'MEDIUM' in product.name.upper()
                        else 'XL' if 'EXTRA LARGE' in
                        product.name.upper()
                        else 'LARGE' if 'LARGE' in product.name.upper()
                        else '',
                        'prices': attribute_prices,
                        'materials': ' / '.join(materials),
                        'product': product,
                        'currency_id': company_id.currency_id,
                    }
                }]

        elif self.name == 'product.product.labels':
            for product in self.pool['product.product'].browse(
                    self.cr, self.uid, self.ids):
                group_label.update({
                    i: {
                        'default_code': product.default_code and
                        product.default_code or
                        product.name and
                        product.name or False,
                        'name': product.product_tmpl_id.name and
                                product.product_tmpl_id.name[:32] or '',
                    }
                })
                i += 1
                if i == group_length:
                    labels += [group_label]
                    group_label = {}
                    i = 0
            if 0 < i < group_length:
                for f in range(i, group_length, 1):
                    group_label.update({
                        f: {
                            'default_code': '',
                            'name': '',
                        }
                    })
                labels += [group_label]
        elif self.name == 'product.attribute.labels':
            for attribute in self.pool['product.attribute'].browse(
                    self.cr, self.uid, self.ids):
                for value in attribute.value_ids.sorted(key=lambda r: r.code):
                    group_label.update({
                        i: {
                            'default_code': (attribute.code if not
                            attribute.is_finishing else '') + value.code if
                            attribute.code and value.code else False,
                        }
                    })
                    i += 1
                    if i == group_length:
                        labels += [group_label]
                        group_label = {}
                        i = 0
            if 0 < i < group_length:
                for f in range(i, group_length, 1):
                    group_label.update({
                        f: {
                            'default_code': '',
                        }
                    })
                labels += [group_label]
        return labels

    def _get_prod_stitching(self, product_id):
        stiching_attribute_id = self.pool['product.attribute'].search(
            self.cr, self.uid, [('code', '=', 'ST')])
        if stiching_attribute_id and product_id.attribute_value_ids:
            for value in product_id.attribute_value_ids:
                if value.attribute_id.id == stiching_attribute_id[0]:
                    return value.attribute_id.name + ' ' + value.name
        return ''

    def _get_prod_leather(self, product_id):
        stiching_attribute_id = self.pool['product.attribute'].search(
            self.cr, self.uid, [('code', '=', 'ST')])
        if stiching_attribute_id and product_id.attribute_value_ids.filtered(
                lambda x: x.attribute_id.id == stiching_attribute_id[0]):
            for value in product_id.attribute_value_ids:
                if value.attribute_id.id != stiching_attribute_id[0]:
                    return value.attribute_id.name + ' ' + value.name
        elif product_id.attribute_value_ids:
            value = product_id.attribute_value_ids[0]
            return value.attribute_id.name + '\n' + value.name
        return product_id.product_tmpl_id.name[32:64]
