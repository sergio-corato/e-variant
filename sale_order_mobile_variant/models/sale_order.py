# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api, _, exceptions, tools
import openerp.addons.decimal_precision as dp
import re


class SaleOrder(models.Model):
    _inherit = ['sale.order', "product.configurator"]
    _name = "sale.order"

    @api.multi
    def print_mobile_quotation(self):
        return self.env['report'].get_action(
            self, 'sale.report_saleorder_mobile_variant')

    product_template_id = fields.Many2one('product.template')
    product_template_image = fields.Binary(
        related='product_template_id.image_medium')
    product_template_image_bis = fields.Binary(
        related='product_template_id.image_medium')
    product_attribute_line_stitching_id = fields.Many2one(
        'product.attribute.line',
        domain="[('product_tmpl_id','=',product_template_id)]")
    stitching_value_ids = fields.Many2many(
        string='Values',
        related='product_attribute_line_stitching_id.value_ids',
        readonly=True)
    stitching_value_id = fields.Many2one(
        'product.attribute.value',
        domain="[('id', 'in', stitching_value_ids[0][2])]")
    product_attribute_child_ids = fields.One2many(
        string='Childs',
        related='product_attribute_line_id.attribute_id.child_ids',
        readonly=True)
    product_attribute_child_id = fields.Many2one(
        'product.attribute',
        domain="[('id', 'in', product_attribute_child_ids[0][2])]")

    product_attribute_value_finishing_id = fields.Many2one(
        'product.attribute.value')

    product_attribute_line_id = fields.Many2one(
        'product.attribute.line',
        domain="[('product_tmpl_id','=',product_template_id)]")
    product_attribute_value_ids = fields.Many2many(
        string='Values',
        related='product_attribute_line_id.value_ids',
        readonly=True)
    product_attribute_value_id = fields.Many2one(
        'product.attribute.value',
        domain="[('id', 'in', product_attribute_value_ids[0][2])]")

    product_attribute_line1_id = fields.Many2one(
        'product.attribute.line',
        domain="[('product_tmpl_id','=',product_template_id)]")
    product_attribute_value1_ids = fields.Many2many(
        string='Values',
        related='product_attribute_line1_id.value_ids',
        readonly=True)
    product_attribute_value1_id = fields.Many2one(
        'product.attribute.value',
        domain="[('id', 'in', product_attribute_value1_ids[0][2])]")

    product_attribute_line2_id = fields.Many2one(
        'product.attribute.line',
        domain="[('product_tmpl_id','=',product_template_id)]")
    product_attribute_value2_ids = fields.Many2many(
        string='Values',
        related='product_attribute_line2_id.value_ids',
        readonly=True)
    product_attribute_value2_id = fields.Many2one(
        'product.attribute.value',
        domain="[('id', 'in', product_attribute_value2_ids[0][2])]")

    product_attribute_line3_id = fields.Many2one(
        'product.attribute.line',
        domain="[('product_tmpl_id','=',product_template_id)]")
    product_attribute_value3_ids = fields.Many2many(
        string='Values',
        related='product_attribute_line3_id.value_ids',
        readonly=True)
    product_attribute_value3_id = fields.Many2one(
        'product.attribute.value',
        domain="[('id', 'in', product_attribute_value3_ids[0][2])]")

    product_attribute_line4_id = fields.Many2one(
        'product.attribute.line',
        domain="[('product_tmpl_id','=',product_template_id)]")
    product_attribute_value4_ids = fields.Many2many(
        string='Values',
        related='product_attribute_line4_id.value_ids',
        readonly=True)
    product_attribute_value4_id = fields.Many2one(
        'product.attribute.value',
        domain="[('id', 'in', product_attribute_value4_ids[0][2])]")

    product_attribute_line5_id = fields.Many2one(
        'product.attribute.line',
        domain="[('product_tmpl_id','=',product_template_id)]")
    product_attribute_value5_ids = fields.Many2many(
        string='Values',
        related='product_attribute_line5_id.value_ids',
        readonly=True)
    product_attribute_value5_id = fields.Many2one(
        'product.attribute.value',
        domain="[('id', 'in', product_attribute_value5_ids[0][2])]")

    product_attribute_image = fields.Binary(
        related='product_attribute_value_id.image')
    product_attribute_image_bis = fields.Binary(
        related='product_attribute_value_id.image')
    product_attribute_image1 = fields.Binary(
        related='product_attribute_value1_id.image')
    price_unit = fields.Float(string='Price unit',
                              digits_compute=dp.get_precision('Product Price'))
    product = fields.Char()
    product_readonly = fields.Char(related='product')
    product_qty = fields.Float(
        string='Q.ty',
        default=1)
    scan = fields.Char('Scan or write QR Code')
    stitching = fields.Selection([
        ('same', 'Same color of material'),
        ('05', 'White'),
        ('manual', 'Selected by qr-code'),
    ], string='Stitching', default='same')
    image = fields.Binary('Image')
    image_medium = fields.Binary(
        'Image Medium', compute='_get_image', inverse='_set_image')
    image1 = fields.Binary('Image1')
    image_medium1 = fields.Binary(
        'Image Medium1', compute='_get_image1', inverse='_set_image1')
    image2 = fields.Binary('Image2')
    image_medium2 = fields.Binary(
        'Image Medium2', compute='_get_image2', inverse='_set_image2')
    is_combination = fields.Selection(
        related='product_template_id.is_combination')
    has_finishing = fields.Boolean(
        related='product_template_id.has_finishing')

    @api.multi
    def onchange_partner_id(self, part):
        res = super(SaleOrder, self).onchange_partner_id(part)
        if self._context.get('params', False):
            if self._context['params'].get('action', False) == \
                    self.env['ir.model.data'].get_object_reference(
                        'sale_order_mobile_variant',
                        'action_sale_order_mobile_variant')[1]:
                if not self.incoterm:
                    res['value'].update(
                        {'incoterm': self.env.ref('stock.incoterm_EXW')})
                if not self.client_order_ref:
                    res['value'].update(
                        {'client_order_ref': 'MAISON ET OBJET - %s %s' % (
                            'SEPT.' if fields.Date.from_string(
                                fields.Date.today()).month == 9 else 'JAN.',
                            fields.Date.from_string(fields.Date.today()).year,
                        )}
                    )
        return res

    @api.multi
    def _get_image(self):
        for record in self:
            record.image_medium = record.image

    @api.multi
    def _get_image1(self):
        for record in self:
            record.image_medium1 = record.image1

    @api.multi
    def _get_image2(self):
        for record in self:
            record.image_medium2 = record.image2

    @api.multi
    def _set_image(self):
        for record in self:
            record.image = tools.image_resize_image_big(record.image_medium)

    @api.multi
    def _set_image1(self):
        for record in self:
            record.image1 = tools.image_resize_image_big(record.image_medium1)

    @api.multi
    def _set_image2(self):
        for record in self:
            record.image2 = tools.image_resize_image_big(record.image_medium2)

    @api.onchange('scan')
    def _scan(self):
        if self.scan:
            # FIRST check if it is a product template (x letter-number)
            product_template = self.env['product.template'].search(
                [('prefix_code', '=', self.scan.upper())])
            if product_template:
                self._set_product_template(product_template)
                self.scan = ''
                return

            # FIRST-1 check if finishing
            if self.has_finishing:
                attribute = False
                attribute_value = self.env['product.attribute.value'].search(
                    [('code', '=', self.scan.upper())])
                if attribute_value:
                    if len(attribute_value) > 1:
                        raise exceptions.ValidationError(
                            'Product with combination malconfigured with at '
                            'least 1 attribute value duplicated!'
                        )
                    attribute = attribute_value.attribute_id
                    self.product_attribute_value_finishing_id = attribute_value
                if attribute:
                    material = attribute.code
                    color = attribute_value.code
                    self._set_material_color(material, color)
                    self.scan = ''
                    return

            # TWO check if it is a material-color (1 letter 2 number)
            if re.match('[A-Z][0-9][0-9]', self.scan.upper()):
                material = self.scan[0].upper()
                color = self.scan[1:3]
                self._set_material_color(material, color)
                self.scan = ''
                return

            # THREE check stitching (ST + 2 numbers) - only 3 qr types
            if re.match('ST[0-9][0-9]', self.scan.upper()):
                self._get_stitching(self.scan[-2:], code_stitching='ST')
                self.scan = ''
                return

            # FOUR check forced material (& + 1 letter + [1 letter + 2 number]
            #  + '+' + [1 letter + 2 number])
            # FIVE check simple material (2 number + 1 or 2 letter + 2 number)
            # SIX check custom material (
            # 1 simbol in &@#£$§°€ + 1 letter + 2 number)
            if re.match('[&][A-Z][A-Z][0-9]{2}[+][A-Z][0-9]{2}',
                        self.scan.upper()) or \
                    re.match('[0-9]{2}[A-Z]{1,2}[0-9]{2}', self.scan.upper())\
                    or re.match('[&@#£$§°€][A-Z][0-9]{2}', self.scan.upper()):
                material = self.scan[:2].upper()
                color = self.scan[2:].upper()
                self._set_material_color(material, color)
                self.scan = ''
                return

            # NO MATCHES FOUND: clean all fields
            self.product_attribute_line_id = \
                self.product_attribute_line1_id = \
                self.product_attribute_line2_id = \
                self.product_attribute_line3_id = \
                self.product_attribute_line4_id = \
                self.product_attribute_line5_id = \
                self.product_attribute_child_id = \
                self.product_attribute_value_id = \
                self.product_attribute_value1_id = \
                self.product_attribute_value2_id = \
                self.product_attribute_value3_id = \
                self.product_attribute_value4_id = \
                self.product_attribute_value5_id = \
                self.product_attribute_value_finishing_id = \
                self.product_template_id = \
                self.product = False
            self.scan = ''

    @api.multi
    def _set_product_template(self, product_template):
        self.product_template_id = product_template
        self.product = product_template.prefix_code
        self.price_unit = 0.0
        # clean all children fields
        self.product_attribute_line_id = \
            self.product_attribute_line1_id = \
            self.product_attribute_line2_id = \
            self.product_attribute_line3_id = \
            self.product_attribute_line4_id = \
            self.product_attribute_line5_id = \
            self.product_attribute_child_id = \
            self.product_attribute_value_id = \
            self.product_attribute_value1_id = \
            self.product_attribute_value2_id = \
            self.product_attribute_value3_id = \
            self.product_attribute_value4_id = \
            self.product_attribute_value5_id = \
            self.product_attribute_value_finishing_id = \
            False

    @api.multi
    def _set_material_color(self, material, color):
        attribute = self.env['product.attribute'].search(
            [('code', '=', material)])
        # first search if material has parent
        if attribute and attribute.parent_id and not self.is_combination:
            # it's a child attribute: set in one passage the attribute line
            # from parent of attribute, and the product_attribute_child_id
            for attribute_line in self.product_template_id. \
                    attribute_line_ids:
                product_attribute_child = \
                    attribute_line.attribute_id.child_ids.filtered(
                        lambda x: x.code == material)
                if product_attribute_child:
                    if not (self.has_finishing and attribute.is_finishing):
                        self.product_attribute_child_id = \
                            product_attribute_child
                        self.product_attribute_line_id = self. \
                            product_template_id.attribute_line_ids.filtered(
                                lambda x: x.attribute_id ==
                                product_attribute_child.parent_id)
                    if self.product_attribute_value_finishing_id:
                        self.product = self.product_template_id.prefix_code + \
                            self.product_attribute_value_finishing_id.code
                    else:
                        self.product = self.product_template_id.prefix_code + \
                            product_attribute_child.code
                    self._get_color(color, self.product_attribute_line_id)
                    if self.stitching == 'same':
                        self._get_stitching(
                            self.product_attribute_value_id.code,
                            code_stitching='ST')
                        return
                    if self.stitching == '05':
                        self._get_stitching(
                            self.stitching,
                            code_stitching='ST')
                        return
                    return
        # then search for combinations
        # att: not set self.product_attribute_child_id even for children of
        # categories, otherwise _get_color function will be bypassed!
        if attribute and self.is_combination and not self.has_finishing:
            if self.product_attribute_line4_id:
                # is already chosen 5 material, so add the 6
                product_attribute_line5 = self.product_template_id. \
                    attribute_line_ids.filtered(
                    lambda x: x.attribute_id.code == material)
                if not self.product_attribute_value1_id:
                    self.product = ''
                if product_attribute_line5:
                    self.product_attribute_line5_id = product_attribute_line5
                    self._get_color(color, self.product_attribute_line5_id)
                    return
            elif self.product_attribute_line3_id:
                # is already chosen 4 material, so add the 5
                product_attribute_line4 = self.product_template_id. \
                    attribute_line_ids.filtered(
                    lambda x: x.attribute_id.code == material)
                if not self.product_attribute_value1_id:
                    self.product = ''
                if product_attribute_line4:
                    self.product_attribute_line4_id = product_attribute_line4
                    self._get_color(color, self.product_attribute_line4_id)
                    return
            elif self.product_attribute_line2_id:
                # is already chosen 3 material, so add the 4
                product_attribute_line3 = self.product_template_id. \
                    attribute_line_ids.filtered(
                    lambda x: x.attribute_id.code == material)
                if not self.product_attribute_value1_id:
                    self.product = ''
                if product_attribute_line3:
                    self.product_attribute_line3_id = product_attribute_line3
                    self._get_color(color, self.product_attribute_line3_id)
                    return
            elif self.product_attribute_line1_id:
                # is already chosen second material, so add the third
                product_attribute_line2 = self.product_template_id. \
                    attribute_line_ids.filtered(
                    lambda x: x.attribute_id.code == material)
                if not self.product_attribute_value1_id:
                    self.product = ''
                if product_attribute_line2:
                    self.product_attribute_line2_id = product_attribute_line2
                    self._get_color(color, self.product_attribute_line2_id)
                    return
            # is already chosen first material but not the 2nd so add the 2nd
            elif self.product_attribute_line_id:
                product_attribute_line1 = self.product_template_id. \
                    attribute_line_ids.filtered(
                    lambda x: x.attribute_id.code == material)
                if not self.product_attribute_value_id:
                    self.product = ''
                if product_attribute_line1:
                    self.product_attribute_line1_id = product_attribute_line1
                    self._get_color(color, self.product_attribute_line1_id)
                    return
            # is not chosen first material
            else:
                product_attribute_line = self.product_template_id. \
                    attribute_line_ids.filtered(
                    lambda x: x.attribute_id.code == material)
                if product_attribute_line:
                    self.product_attribute_line_id = product_attribute_line
                    self._get_color(color, self.product_attribute_line_id)
                    return
        # is the first material, work as usual
        if attribute and not attribute.parent_id and not self.is_combination:
            product_attribute_line = self.product_template_id. \
                attribute_line_ids.filtered(
                    lambda x: x.attribute_id.code == material)
            if product_attribute_line:
                self.product_attribute_line_id = product_attribute_line
                self._get_color(color, self.product_attribute_line_id)
                return

    def _get_color(self, color, product_attribute_line_id):
        if self.product_attribute_child_id:
            product_attribute_value = self.product_attribute_child_id. \
                value_ids.filtered(lambda x: x.code == color)
        else:
            product_attribute_value = product_attribute_line_id.attribute_id.\
                value_ids.filtered(lambda x: x.code == color)
        if product_attribute_value:
            if self.product_attribute_value4_id and self.is_combination:
                self.product_attribute_value5_id = product_attribute_value
            elif self.product_attribute_value3_id and self.is_combination:
                self.product_attribute_value4_id = product_attribute_value
            elif self.product_attribute_value2_id and self.is_combination:
                self.product_attribute_value3_id = product_attribute_value
            elif self.product_attribute_value1_id and self.is_combination:
                self.product_attribute_value2_id = product_attribute_value
            elif self.product_attribute_value_id and self.is_combination:
                self.product_attribute_value1_id = product_attribute_value
            else:
                self.product_attribute_value_id = product_attribute_value
            product = False
            # we can search here for product only if not with child variant
            if not self.product_attribute_child_id.parent_id \
                    and not self.is_combination:
                product = self.env['product.product'].search([
                    ('product_tmpl_id', '=', self.product_template_id.id),
                    ('attribute_value_ids', '=',
                     self.product_attribute_value_id.id)
                ])
            if product:
                self.product = product.default_code
            else:
                if self.product_attribute_child_id:
                    self.product = self.product_template_id.prefix_code + (
                        self.product_attribute_value_finishing_id.code
                        if self.product_attribute_value_finishing_id else ''
                    ) + \
                        self.product_attribute_child_id.code + \
                        product_attribute_value.code
                else:
                    if self.product_attribute_line1_id and \
                            not self.product_attribute_line2_id and \
                            not self.product_attribute_line3_id and \
                            not self.product_attribute_line4_id and \
                            not self.product_attribute_line5_id:
                        self.product = self.product_template_id.prefix_code + (
                            self.product_attribute_value_finishing_id.code
                            if self.product_attribute_value_finishing_id else
                            ''
                        ) + \
                            self.product_attribute_line_id.attribute_id.code +\
                            self.product_attribute_value_id.code + \
                            self.product_attribute_line1_id.attribute_id.code + \
                            product_attribute_value.code
                    elif self.product_attribute_line1_id and \
                            self.product_attribute_line2_id and \
                            not self.product_attribute_line3_id and \
                            not self.product_attribute_line4_id and \
                            not self.product_attribute_line5_id:
                        self.product = self.product_template_id.prefix_code + (
                            self.product_attribute_value_finishing_id.code
                            if self.product_attribute_value_finishing_id else
                            '') + \
                        self.product_attribute_line_id.attribute_id.code + \
                        self.product_attribute_value_id.code + \
                        self.product_attribute_line1_id.attribute_id.code + \
                        self.product_attribute_value1_id.code + \
                        self.product_attribute_line2_id.attribute_id.code+\
                        product_attribute_value.code
                    elif self.product_attribute_line1_id and \
                            self.product_attribute_line2_id and \
                            self.product_attribute_line3_id and \
                            not self.product_attribute_line4_id and \
                            not self.product_attribute_line5_id:
                        self.product = self.product_template_id.prefix_code + (
                            self.product_attribute_value_finishing_id.code
                            if self.product_attribute_value_finishing_id else
                            '') + \
                        self.product_attribute_line_id.attribute_id.code + \
                        self.product_attribute_value_id.code + \
                        self.product_attribute_line1_id.attribute_id.code + \
                        self.product_attribute_value1_id.code + \
                        self.product_attribute_line2_id.attribute_id.code + \
                        self.product_attribute_value2_id.code + \
                        self.product_attribute_line3_id.attribute_id.code + \
                        product_attribute_value.code
                    elif self.product_attribute_line1_id and \
                            self.product_attribute_line2_id and \
                            self.product_attribute_line3_id and \
                            self.product_attribute_line4_id and \
                            not self.product_attribute_line5_id:
                        self.product = self.product_template_id.prefix_code + (
                            self.product_attribute_value_finishing_id.code
                            if self.product_attribute_value_finishing_id else
                            '') + \
                        self.product_attribute_line_id.attribute_id.code + \
                        self.product_attribute_value_id.code + \
                        self.product_attribute_line1_id.attribute_id.code + \
                        self.product_attribute_value1_id.code + \
                        self.product_attribute_line2_id.attribute_id.code + \
                        self.product_attribute_value2_id.code + \
                        self.product_attribute_line3_id.attribute_id.code + \
                        self.product_attribute_value3_id.code + \
                        self.product_attribute_line4_id.attribute_id.code + \
                        product_attribute_value.code
                    elif self.product_attribute_line1_id and \
                            self.product_attribute_line2_id and \
                            self.product_attribute_line3_id and \
                            self.product_attribute_line4_id and \
                            self.product_attribute_line5_id:
                        self.product = self.product_template_id.prefix_code + (
                            self.product_attribute_value_finishing_id.code
                            if self.product_attribute_value_finishing_id else
                            '') + \
                        self.product_attribute_line_id.attribute_id.code + \
                        self.product_attribute_value_id.code + \
                        self.product_attribute_line1_id.attribute_id.code + \
                        self.product_attribute_value1_id.code + \
                        self.product_attribute_line2_id.attribute_id.code + \
                        self.product_attribute_value2_id.code + \
                        self.product_attribute_line3_id.attribute_id.code + \
                        self.product_attribute_value3_id.code + \
                        self.product_attribute_line4_id.attribute_id.code + \
                        self.product_attribute_value4_id.code + \
                        self.product_attribute_line5_id.attribute_id.code + \
                        product_attribute_value.code
                    else:
                        self.product = self.product_template_id.prefix_code + (
                            self.product_attribute_value_finishing_id.code
                            if self.product_attribute_value_finishing_id else
                            '') + \
                            self.product_attribute_line_id.attribute_id.code +\
                            product_attribute_value.code
            self.price_unit = self._get_price_unit()
        else:
            self.product_attribute_value_id = False

    def _get_stitching(self, stitching, code_stitching):
        if self.product_attribute_child_id and self.product_attribute_value_id:
            product_attribute_line = self.product_template_id.\
                attribute_line_ids.filtered(
                    lambda x: x.attribute_id.code == code_stitching)
            if product_attribute_line:
                self.product_attribute_line_stitching_id = \
                    product_attribute_line
                stitching_id = product_attribute_line.value_ids.filtered(
                    lambda x: x.code == stitching
                )
                if stitching_id:
                    self.stitching_value_id = stitching_id
                    self.product = self.product_template_id.prefix_code + (
                        self.product_attribute_value_finishing_id.code
                        if self.product_attribute_value_finishing_id else '') \
                        + self.product_attribute_child_id.code + \
                        self.product_attribute_value_id.code + \
                        self.product_attribute_line_stitching_id.attribute_id.\
                        code + stitching_id.code
            else:
                self.product_attribute_line_stitching_id = False

    @api.onchange('stitching')
    def onchange_stitching(self):
        if self.product_attribute_value_id and self.stitching:
            if self.stitching == '05':
                self._get_stitching(self.stitching, code_stitching='ST')
            if self.stitching == 'same':
                self._get_stitching(
                    self.product_attribute_value_id.code,
                    code_stitching='ST')

    @api.multi
    def _get_price_unit(self):
        self.ensure_one()
        if self.product_template_id:
            price_extra = 0.0
            attribute_id = False
            for attr_line in self.product_attribute_line_id:
                price_extra += attr_line.price_extra
                # if attr_line.value_id:
                #     attribute_id = attr_line.attribute_id
            for attr_line1 in self.product_attribute_line1_id:
                price_extra += attr_line1.price_extra
            for attr_line2 in self.product_attribute_line2_id:
                price_extra += attr_line2.price_extra
            for attr_line3 in self.product_attribute_line3_id:
                price_extra += attr_line3.price_extra
            for attr_line4 in self.product_attribute_line4_id:
                price_extra += attr_line4.price_extra
            for attr_line5 in self.product_attribute_line5_id:
                price_extra += attr_line5.price_extra
            for attribute_line in self.product_attribute_value_id:
                # if attribute_line.attribute_id == attribute_id:
                price_extra += attribute_line.price_extra
            return self.pricelist_id.with_context({
                'uom': self.product_template_id.uom_id.id,
                'date': self.date_order,
                'price_extra': price_extra,
            }).template_price_get(
                self.product_template_id.id, 1.0,
                self.partner_id.id)[self.pricelist_id.id]

    @api.multi
    def add_product_to_order(self):
        if self.product:
            product_obj = self.env['product.product']
            product_id = False

            # search product - if it is attribute-child
            if self.product_template_id and self.product_attribute_child_id \
                    and self.stitching_value_id and not self.has_finishing:
                domain = product_obj._build_attributes_domain(
                    self.product_template_id, [
                        {'value_id': self.product_attribute_value_id.id},
                        {'value_id': self.stitching_value_id.id}])
                product_id = product_obj.search(domain[0])
                if not product_id:
                    product_id = product_obj.create({
                        'product_tmpl_id': self.product_template_id.id,
                        'attribute_value_ids':
                            [(6, 0,
                              [self.product_attribute_value_id.id,
                               self.stitching_value_id.id])]
                    })

            # search product - if it is with finishing
            if self.product_template_id and self.product_attribute_child_id \
                    and self.stitching_value_id and self.has_finishing:
                domain = product_obj._build_attributes_domain(
                    self.product_template_id, [
                        {'value_id': self.product_attribute_value_id.id},
                        {'value_id': self.stitching_value_id.id},
                        {'value_id': self.product_attribute_value_finishing_id
                            .id}
                    ])
                product_id = product_obj.search(domain[0])
                if not product_id:
                    product_id = product_obj.create({
                        'product_tmpl_id': self.product_template_id.id,
                        'attribute_value_ids':
                            [(6, 0,
                              [self.product_attribute_value_id.id,
                               self.stitching_value_id.id,
                               self.product_attribute_value_finishing_id.id])]
                    })

            # combination type
            if self.product_template_id and self.is_combination:
                list_dict_attribute_values = [
                        {'value_id': self.product_attribute_value_id.id}]
                if self.product_attribute_value1_id:
                    list_dict_attribute_values.append(
                        {'value_id': self.product_attribute_value1_id.id})
                if self.product_attribute_value2_id:
                    list_dict_attribute_values.append(
                        {'value_id': self.product_attribute_value2_id.id})
                if self.product_attribute_value3_id:
                    list_dict_attribute_values.append(
                        {'value_id': self.product_attribute_value3_id.id})
                if self.product_attribute_value4_id:
                    list_dict_attribute_values.append(
                        {'value_id': self.product_attribute_value4_id.id})
                if self.product_attribute_value5_id:
                    list_dict_attribute_values.append(
                        {'value_id': self.product_attribute_value5_id.id})
                domain = product_obj._build_attributes_domain(
                    self.product_template_id, list_dict_attribute_values)
                product_id = product_obj.search(domain[0])
                if not product_id:
                    list_attribute_values = [
                        self.product_attribute_value_id.id]
                    if self.product_attribute_value1_id:
                        list_attribute_values.append(
                            self.product_attribute_value1_id.id)
                    if self.product_attribute_value2_id:
                        list_attribute_values.append(
                            self.product_attribute_value2_id.id)
                    if self.product_attribute_value3_id:
                        list_attribute_values.append(
                            self.product_attribute_value3_id.id)
                    if self.product_attribute_value4_id:
                        list_attribute_values.append(
                            self.product_attribute_value4_id.id)
                    if self.product_attribute_value5_id:
                        list_attribute_values.append(
                            self.product_attribute_value5_id.id)
                    product_id = product_obj.create({
                        'product_tmpl_id': self.product_template_id.id,
                        'attribute_value_ids':
                            [(6, 0, list_attribute_values)]
                    })

            # attribute type only
            if self.product_template_id \
                    and not self.product_attribute_child_id \
                    and not self.is_combination:
                product_id = product_obj.search([
                    ('product_tmpl_id', '=', self.product_template_id.id),
                    ('attribute_value_ids', '=',
                     self.product_attribute_value_id.id)
                ])
                if not product_id:
                    product_id = product_obj.create({
                        'product_tmpl_id': self.product_template_id.id,
                        'attribute_value_ids':
                            [(6, 0,
                              [self.product_attribute_value_id.id])]
                    })

            if product_id:
                if len(product_id) != 1:
                    raise exceptions.ValidationError(
                        'Found more than 1 product, product template has '
                        'malformed variants.'
                    )
                price_unit = self._get_price_unit() or 0.0
                if product_id in self.order_line.mapped('product_id'):
                    line = self.order_line.filtered(
                        lambda r: r.product_id == product_id)
                    line.product_uom_qty += self.product_qty if \
                        self.product_qty else 1
                else:
                    taxes = product_id.taxes_id.filtered(
                        lambda t: t.company_id.id == self.company_id.id)
                    taxes_ids = self.fiscal_position.map_tax(taxes) if \
                        self.fiscal_position else False
                    self.order_line.create({
                        'order_id': self.id,
                        'product_tmpl_id': product_id.product_tmpl_id.id,
                        'product_id': product_id.id,
                        'name': self._get_product_description(
                            product_id.product_tmpl_id, product_id,
                            product_id.attribute_value_ids),
                        'product_uom_qty': self.product_qty if
                        self.product_qty else 1,
                        'price_unit': price_unit,
                        'product_uom': product_id.product_tmpl_id.uom_id.id,
                        'state': 'draft',
                        'delay': 0.0,
                        'discount': self.default_sale_discount,
                        'tax_id': [(6, 0, taxes_ids.ids)],
                    })

                # clean fields invisibles
                self.product_template_id = \
                    self.product_attribute_line_id = \
                    self.product_attribute_line1_id = \
                    self.product_attribute_line2_id = \
                    self.product_attribute_line3_id = \
                    self.product_attribute_line4_id = \
                    self.product_attribute_line5_id = \
                    self.product_attribute_line_stitching_id = \
                    self.stitching_value_id = \
                    self.product_attribute_child_id = \
                    self.product_attribute_value_id = \
                    self.product_attribute_value1_id = \
                    self.product_attribute_value2_id = \
                    self.product_attribute_value3_id = \
                    self.product_attribute_value4_id = \
                    self.product_attribute_value5_id = \
                    self.product_attribute_value_finishing_id = \
                    False
                self.product = ''
