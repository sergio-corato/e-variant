# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import fields, models, exceptions, api, _
import base64
import csv
import cStringIO
import re


class ProductImport(models.TransientModel):
    _name = 'product.variant.create'

    data = fields.Binary('File', required=True)
    name = fields.Char('Filename')
    delimeter = fields.Char('Delimeter', default=',',
                            help='Default delimeter is ","')
    log = fields.Text('Log')
    product_tmpl_malformed_ids = fields.Many2many(
        string='Malformed templates',
        comodel_name='product.template')

    @api.multi
    def action_product_variant_create(self):
        data = base64.b64decode(self.data)
        file_input = cStringIO.StringIO(data)
        file_input.seek(0)
        reader_info = []
        if self.delimeter:
            delimeter = str(self.delimeter)
        else:
            delimeter = ','
        reader = csv.reader(file_input, delimiter=delimeter, lineterminator='\r\n')
        try:
            reader_info.extend(reader)
        except Exception:
            raise exceptions.Warning(_("Not a valid file!"))
        keys = reader_info[0]
        # check if keys exist
        if not isinstance(keys, list) or ('order_line/product_id' not in keys
                                          or 'order_line/product_tmpl_id' not in keys):
            raise exceptions.Warning(
                _("Not 'order_line/product_id' or 'order_line/product_tmpl_id' "
                  "keys found"))
        del reader_info[0]

        product_obj = self.env['product.product']
        product_tmpl_obj = self.env['product.template']
        product_tmpl_malformed_ids = self.env['product.template']
        self.log = ''
        for i in range(len(reader_info)):
            field = reader_info[i]
            values = dict(zip(keys, field))
            product_code = values['order_line/product_id'].upper()
            template_code = values['order_line/product_tmpl_id'].upper()
            if ' ' in product_code or ' ' in template_code or \
                    re.search(r'\s\s+', product_code) is not None \
                    or re.search(r'\s\s+', template_code) is not None:
                self.log += 'Trailing space in code "%s" or template code "%s", ' \
                            'ignored. Fix csv for a valid import!' % (
                                product_code, template_code
                            )
            product = product_obj.search([
                ('default_code', '=', product_code)
            ])
            product_tmpl_id = product_tmpl_obj.search([
                ('prefix_code', '=', template_code)
            ])
            if product_tmpl_id and not product:
                # get only attribute part of the code
                attribute_code = product_code.replace(template_code, '')
                # get and remove stitching part of the code if exists
                stitching = False
                code_stitching = False
                stitching_attribute = re.search(
                    '[S&][T&][0-9]{2}$', attribute_code)
                if stitching_attribute is not None and stitching_attribute.group(0):
                    stitching_group = stitching_attribute.group(0)
                    stitching = stitching_group[0:2]
                    code_stitching = stitching_group[2:4]
                    # get material attributes removing stitching attribute
                    material_attributes = attribute_code.replace(stitching_group, '')
                else:
                    # no stitching attribute, so it is the whole code
                    material_attributes = attribute_code
                # search materials in material attributes
                mat_color_dict = []
                child_attributes = re.findall(
                    '[&@#£$§°€]?[A-Z]{1,2}[0-9]{2}', material_attributes)
                # get all material - color from founds child attributes
                if child_attributes:
                    for child in child_attributes:
                        # material_attributes = child_attributes.group(0)
                        special_mat = re.search('[&@#£$§°€][A-Z]{1,2}', child)
                        if special_mat:
                            mat = special_mat.group(0)
                        else:
                            mat = re.search('[A-Z]{1,2}', child).group(0)
                        color = re.search('[0-9]{1,2}', child).group(0)
                        if mat and color:
                            mat_color_dict.append({
                                'mat': mat,
                                'color': color,
                            })

                if not mat_color_dict:
                    self.log += 'Missing materials for code %s\n' % product_code
                    product_tmpl_malformed_ids |= product_tmpl_id
                    continue
                product_attribute_value_ids = self.env['product.attribute.value']
                for material in mat_color_dict:
                    try:
                        product_attribute_value_id = \
                            self.get_material_color(
                                product_tmpl_id, material['mat'], material['color'])
                        if product_attribute_value_id:
                            product_attribute_value_ids |= product_attribute_value_id
                    except:
                        self.log += 'Product code not interpretable %s\n' % product_code
                        product_tmpl_malformed_ids |= product_tmpl_id
                        continue
                ignored_stitching = False
                stitching_id = False
                if stitching and code_stitching:
                    stitching_id = self.get_stitching(
                        product_tmpl_id, stitching, code_stitching)
                    if not stitching_id:
                        ignored_stitching = True
                if ignored_stitching:
                    self.log += 'Stitching not valid for %s, ignored. ' \
                                'Fix csv for a valid import!\n' \
                        % product_code
                if product_attribute_value_ids:
                    attr_ids = product_attribute_value_ids.ids
                    product_values = {
                        'product_tmpl_id': product_tmpl_id.id,
                        'attribute_value_ids': [
                            (6, 0, attr_ids)]
                    }
                    if stitching_id:
                        attr_ids.append(stitching_id.id)
                        product_values.update({
                            'attribute_value_ids': [(6, 0, attr_ids)]
                        })
                    # il codice è scritto con lo stitching anche se non esiste nella
                    # configurazione del template per cui lo cerco senza stitching_group
                    elif stitching:
                        product = product_obj.search([
                            ('default_code', '=', product_code.replace(
                                stitching_group, ''
                            ))
                        ])
                    if not product:
                        # material attributes can be written in other ways, so
                        # MAP015G57F65A20 is equal to MAP015A20F65G57
                        domain = [('product_tmpl_id', '=', product_tmpl_id.id)]
                        for attr_id in attr_ids:
                            domain.append(('attribute_value_ids', '=', attr_id))
                        product = product_obj.search(domain)
                        if product:
                            self.log += 'Product variant written in format %s instead '\
                                        'of already existing one %s\n' % (
                                            product_code, product.default_code
                                        )
                    if not product:
                        product_obj.create(product_values)
        self.product_tmpl_malformed_ids = product_tmpl_malformed_ids

    @api.multi
    def get_material_color(self, product_template, material, color):
        attribute = self.env['product.attribute'].search([('code', '=', material)])
        # first search if material has parent
        product_attribute_value_id = False
        if attribute and attribute.parent_id:
            # it's a child attribute: set in one passage the attribute line
            # from parent of attribute, and the product_attribute_child_id
            for attribute_line in product_template.attribute_line_ids:
                ignored_stitching = False
                product_attribute_child_id = \
                    attribute_line.attribute_id.child_ids.filtered(
                        lambda x: x.code == material)
                if product_attribute_child_id:
                    # this is a normal attribute with childs (aka CAT. A)
                    product_attribute_line_id = \
                        product_template.attribute_line_ids.filtered(
                            lambda x: x.attribute_id ==
                            product_attribute_child_id.parent_id)
                    if product_attribute_line_id:
                        product_attribute_value_id = self.get_color(
                            color, product_attribute_child_id,
                            product_attribute_line_id)
                else:
                    # this is a combination or normal attribute with values (
                    # aka 10 TECNOCUOIO)
                    product_attribute_line_id = \
                        attribute_line.attribute_id.filtered(
                            lambda x: x.code == material)
                    if product_attribute_line_id:
                        product_attribute_value_id = self.get_color(
                            color, False, product_attribute_line_id)
                if product_attribute_value_id:
                    break

        elif attribute and not attribute.parent_id:
            product_attribute_line_id = product_template.attribute_line_ids.filtered(
                lambda x: x.attribute_id.code == material)
            if product_attribute_line_id:
                product_attribute_value_id = self.get_color(
                    color, False, product_attribute_line_id)

        return product_attribute_value_id

    @api.multi
    def get_color(self, color, product_attribute_child_id=False,
                  product_attribute_line_id=False):
        product_attribute_value = False
        if product_attribute_child_id:
            product_attribute_value = product_attribute_child_id.value_ids.filtered(
                lambda x: x.code == color)
        elif product_attribute_line_id:
            product_attribute_value = product_attribute_line_id.value_ids.filtered(
                lambda x: x.code == color)
        if product_attribute_value:
            return product_attribute_value
        else:
            raise exceptions.ValidationError('Material color not found')

    @api.multi
    def get_stitching(self, product_template, stitching, code_stitching):
        product_attribute_line = product_template.attribute_line_ids.filtered(
            lambda x: x.attribute_id.code == stitching)
        if product_attribute_line:
            stitching_id = product_attribute_line.value_ids.filtered(
                lambda x: x.code == code_stitching
            )
            if stitching_id:
                return stitching_id
        return False
