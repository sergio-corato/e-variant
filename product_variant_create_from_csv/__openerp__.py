# -*- coding: utf-8 -*-
# Copyright 2020 Sergio Corato <https://github.com/sergiocorato>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "Product Variant Create from CSV file",
    "version": "8.0.1.0.0",
    "category": "Generic Modules",
    "license": "AGPL-3",
    "author": "Sergio Corato",
    "description": "Create missing variants to easy mass import.",
    "contributors": [
        "Sergio Corato",
    ],
    "website": "https://efatto.it",
    "depends": [
        "product",
    ],
    "data": [
        "wizard/product_variant_create.xml",
    ],
    "installable": True,
}
