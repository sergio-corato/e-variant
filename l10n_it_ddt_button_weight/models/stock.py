# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api


class StockPickingPackagePreparation(models.Model):
    _inherit = "stock.picking.package.preparation"

    @api.multi
    @api.depends('net_weight_custom')
    def ddt_weight_force(self):
        for ddt in self:
            ddt.weight_custom = ddt.net_weight_custom * 1.25


class StockPackOperation(models.Model):
    _inherit = "stock.pack.operation"

    sale_id = fields.Many2one(
        related='picking_id.sale_id'
    )
