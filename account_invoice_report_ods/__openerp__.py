# -*- coding: utf-8 -*-
# Copyright 2020 Sergio Corato <https://github.com/sergiocorato>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    'name': 'Invoice xls report',
    'version': '8.0.1.0.0',
    'category': 'other',
    'description': """
    Invoice xls report.
    """,
    'author': 'Sergio Corato',
    'website': 'https://efatto.it',
    'license': 'AGPL-3',
    "depends": [
        'account',
        'report_aeroo',
        'report_aeroo_parser',
    ],
    "data": [
        'report/report.xml',
    ],
    "installable": True,
}
