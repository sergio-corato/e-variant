# -*- coding: utf-8 -*-
#
#
#    Copyright (C) 2016-2017 Sergio Corato
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
{
    'name': 'Compute prices of variants on product template attribute',
    'version': '8.0.1.0.0',
    'category': 'other',
    'description': """
With this module price of attribute can be summed to variant prices.
""",
    'author': 'Sergio Corato',
    'website': 'http://www.efatto.it',
    'license': 'AGPL-3',
    "depends": [
        'sale',
        'product_code_builder_parent',
        'product_supplierinfo_variant',
        'purchase',
        'sale_product_variants',
        'visible_discount_sale',
    ],
    'conflicts': [
        'sale_order_recalculate_prices_variants',
        'sale_order_recalculate_prices',
    ],
    "data": [
        'views/product_view.xml',
    ],
    "installable": True
}
