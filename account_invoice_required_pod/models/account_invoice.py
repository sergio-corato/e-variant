# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api


class ResCountryGroup(models.Model):
    _inherit = "res.country.group"

    is_pod_required = fields.Boolean(string='POD is required')


class DdTCreateInvoice(models.TransientModel):
    _inherit = "ddt.create.invoice"

    @api.multi
    def create_invoice(self):
        res = super(DdTCreateInvoice, self).create_invoice()
        domain = res.get('domain')
        for invoice in self.env['account.invoice'].search(domain):
            partner = invoice.partner_id
            if invoice.address_shipping_id:
                partner = invoice.address_shipping_id
            if any(
                [x.is_pod_required for x in partner.country_id.country_group_ids]) and \
                    not invoice.is_advance_invoice:
                invoice.is_pod_required = True
            else:
                invoice.is_pod_required = False
        return res


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    is_pod_required = fields.Boolean(string='POD is required')
    pod_document = fields.Binary(string='File POD document')
    pod_reference = fields.Char('POD reference')
    pod_fname = fields.Char(string='Filename POD document')

    @api.onchange('address_shipping_id')
    def onchange_pod_document(self):
        partner = self.partner_id
        if self.address_shipping_id:
            partner = self.address_shipping_id
        if any([x.is_pod_required for x in partner.country_id.country_group_ids]) and \
                not self.is_advance_invoice:
            self.is_pod_required = True
        else:
            self.is_pod_required = False

    @api.multi
    def onchange_partner_id(self, type, partner_id, date_invoice=False,
                            payment_term=False, partner_bank_id=False,
                            company_id=False):

        result = super(AccountInvoice, self).onchange_partner_id(
            type, partner_id, date_invoice, payment_term,
            partner_bank_id, company_id)

        if not partner_id:
            return result
        partner_id = partner_id
        if self.address_shipping_id:
            partner_id = self.address_shipping_id.id
        partner = self.env['res.partner'].browse(partner_id)
        if any([x.is_pod_required for x in partner.country_id.country_group_ids]) and \
                not self.is_advance_invoice:
            result['value']['is_pod_required'] = True
        else:
            result['value']['is_pod_required'] = False
        return result
