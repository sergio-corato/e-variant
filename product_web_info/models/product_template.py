# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields


class ProductTemplate(models.Model):
    _inherit = "product.template"

    meta_title = fields.Char()
    meta_keywords = fields.Char()
    meta_description = fields.Char()
    web_description = fields.Char()
    related_skus = fields.Many2many(
        comodel_name='product.template',
        relation='product_template_related_skus_rel',
        column1='product_tmpl_id',
        column2='related_skus_id',
        string='Related Skus')
