# -*- coding: utf-8 -*-
# Copyright 2021 Sergio Corato <https://github.com/sergiocorato>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    'name': 'Product template fields for web',
    'version': '8.0.1.0.0',
    'category': 'other',
    'description': """
Add fields for web
""",
    'author': 'Sergio Corato',
    'website': 'http://www.efatto.it',
    'license': 'AGPL-3',
    "depends": [
        'product',
    ],
    "data": [
        'views/product_view.xml',
    ],
    "installable": True
}
