# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api, exceptions, _


class ProductProduct(models.Model):
    _inherit = "product.product"

    att_image = fields.Binary(
        string="Attribute Image",
        compute='_compute_att_image',
        store=False)

    @api.multi
    def _compute_att_image(self):
        for product in self:
            attribute_value_id = product.attribute_value_ids.filtered(
                lambda x: not x.attribute_id.is_finishing
            )
            for value in attribute_value_id:
                if value.image:
                    product.att_image = value.image
                    break

    @api.multi
    def _compute_default_code(self):
        for record in self:
            if not record.product_tmpl_id \
                    .attribute_line_ids:
                record.default_code = record.prefix_code
            elif record.auto_default_code:
                record.default_code = record._get_default_code()
            else:
                record.default_code = record.manual_default_code

    @api.multi
    def _inverse_default_code(self):
        for record in self:
            if not record.product_tmpl_id \
                    .attribute_line_ids:
                record.prefix_code = record.default_code
            elif not record.auto_default_code:
                record.manual_default_code = record.default_code
            else:
                raise exceptions.Warning(
                    _('Default can no be set manually as the product '
                      'is configured to have a computed code'))


class ProductAttribute(models.Model):
    _inherit = "product.attribute"

    code = fields.Char(
        'Code', required=True,
        help='If Code in Report is set, the part of this code matching a single '
             'special char in &@#£$§°€ followed by a capital letter, will be '
             'replaced by Code in Report. If Code in Report is equal to the string '
             'False, this will replaced with an empty string.'
    )
    parent_id = fields.Many2one('product.attribute')
    child_ids = fields.One2many(
        'product.attribute',
        'parent_id',
        'Child Attributes'
    )
    value_ids = fields.One2many(
        'product.attribute.value',
        'attribute_id',
        'Attribute Values'
    )
    code_in_report = fields.Char('Code in Report')
    is_finishing = fields.Boolean()

    @api.one
    def copy(self, default=None):
        raise Warning(_("Attribute can't be duplicated"))

    @api.multi
    def name_get(self):
        """Use the code and name as name."""
        res = []
        for record in self:
            res.append(
                (record.id, "%s - %s" % (record.code,
                                         record.name)))
        return res


class ProductAttributeValue(models.Model):
    _inherit = "product.attribute.value"

    code = fields.Char('Code', required=True)

    @api.multi
    def name_get(self):
        """Use the code and name as name."""
        res = []
        for record in self:
            res.append(
                (record.id, "%s - %s" % (record.code,
                                         record.name)))
        return res

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        ids = self.search([('code', 'ilike', name)] + args,
                          limit=limit)
        if ids:
            return ids.name_get()
        return super(ProductAttributeValue, self).name_search(
            name=name, args=args, operator=operator, limit=limit)
