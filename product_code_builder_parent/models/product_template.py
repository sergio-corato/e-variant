# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api
import psycopg2
from openerp import tools
from openerp.exceptions import Warning as UserError


class ProductTemplate(models.Model):
    _inherit = "product.template"
    _order = 'sequence,prefix_code,name'

    sequence = fields.Integer('Sequence', default=100)
    is_combination = fields.Selection([
        ('combination2', 'Combination of 2 materials'),
        ('combination3', 'Combination of 3 materials')
    ], string='Is a combination',
        help='Material - not categories - can be combined directly, without '
        'stitching. Selected one of the available types.')
    has_finishing = fields.Boolean()

    @api.model
    def create(self, vals):
        res = super(ProductTemplate, self).create(vals)
        if vals.get('attribute_line_ids', False):
            attr_ids = []
            for attr_line in vals['attribute_line_ids']:
                attr_ids.append(attr_line[2]['attribute_id'])
            attribute_ids = self.env['product.attribute'].browse(attr_ids)

            # 1 rabitti and #3 forced: all of this
            attribute_line_normal = attribute_ids.filtered(
                lambda x: not x.parent_id and not x.child_ids
                and not x.sequence == 10
            )
            # 4 combination: only many of this (mat with ctg used not properly)
            attribute_line_combination = attribute_ids.filtered(
                lambda x: x.parent_id and not x.child_ids)
            if (attribute_line_normal or attribute_line_combination) and \
                    attribute_ids - attribute_line_normal - \
                    attribute_line_combination:
                raise UserError('Mixed variant types is not possible! '
                                'Normal attributes can\'t be used with '
                                'attribute with childs or parent.')
            # 4 combination: only many of this (mat with ctg used not properly)
            # or of #1
            if attribute_line_combination:
                if not self.is_combination and not vals.get(
                        'is_combination', False):
                    raise UserError(
                        'It seems it is a combination. '
                        'Please check \'Is a combination\' flag')

            # 2 giob: only one of this
            attribute_line_stitching = attribute_ids. \
                filtered(
                lambda x: not x.parent_id and not x.child_ids
                and x.sequence == 10)
            # 2b giob: many of this ( and many without parent but with childs)
            attribute_line_ctg = attribute_ids.filtered(
                lambda x: not x.parent_id and x.child_ids)
            if attribute_line_ctg and len(attribute_line_stitching) != 1:
                raise UserError('Mixed variant types is not possible! '
                                'One and only one attribute stitching is '
                                'possible with attribute with childs.')

            # check the rest (none I think)
            if attribute_ids - \
                    attribute_line_combination - \
                    attribute_line_ctg - \
                    attribute_line_stitching - \
                    attribute_line_normal:
                raise UserError('Not defined variant type! ')

        return res

    @api.multi
    def write(self, vals):
        res = super(ProductTemplate, self).write(vals)
        if vals.get('attribute_line_ids', False):

            # 1 rabitti and #3 forced: all of this
            attribute_line_normal = self.attribute_line_ids.filtered(
                lambda x: not x.attribute_id.parent_id
                and not x.attribute_id.child_ids
                and not x.attribute_id.sequence == 10
            )
            attribute_line_combination = self.attribute_line_ids.filtered(
                lambda x: x.attribute_id.parent_id
                and not x.attribute_id.child_ids)
            if (attribute_line_normal or attribute_line_combination) and \
                    self.attribute_line_ids - attribute_line_normal - \
                    attribute_line_combination:
                raise UserError('Mixed variant types is not possible! '
                                'Normal attributes can\'t be used with '
                                'attribute with childs or parent.')
            # 4 combination: only many of this (mat with ctg used not properly)
            # or of #1
            if attribute_line_combination:
                if not self.is_combination and not vals.get(
                        'is_combination', False):
                    raise UserError(
                        'It seems it is a combination. '
                        'Please check \'Is a combination\' flag')

            # 2 giob: only one of this
            attribute_line_stitching = self.attribute_line_ids. \
                filtered(
                lambda x: not x.attribute_id.parent_id
                          and not x.attribute_id.child_ids
                          and x.attribute_id.sequence == 10)
            # 2b giob: many of this ( and many without parent but with childs)
            attribute_line_ctg = self.attribute_line_ids.filtered(
                lambda x: not x.attribute_id.parent_id
                          and x.attribute_id.child_ids)
            if attribute_line_ctg and len(attribute_line_stitching) != 1:
                raise UserError('Mixed variant types is not possible! '
                                'One and only one attribute stitching is '
                                'possible with attribute with childs.')

            # check the rest (none I think)
            if self.attribute_line_ids - \
                    attribute_line_combination - \
                    attribute_line_ctg - \
                    attribute_line_stitching - \
                    attribute_line_normal:
                raise UserError('Not defined variant type! ')

        return res

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if args is not None:
            ids = self.search([('prefix_code', 'ilike', name)] + args,
                              limit=limit)
        else:
            ids = self.search([('prefix_code', '=', name)], limit=limit)
        if ids:
            return ids.name_get()
        return super(ProductTemplate, self).name_search(
            name=name, args=args, operator=operator, limit=limit)

    @api.cr_uid_ids_context
    def create_variant_ids(self, cr, uid, ids, context=None):
        product_obj = self.pool.get("product.product")
        ctx = context and context.copy() or {}

        if ctx.get("create_product_variant"):
            return None

        ctx.update(active_test=False, create_product_variant=True)

        tmpl_ids = self.browse(cr, uid, ids, context=ctx)
        for tmpl_id in tmpl_ids:
            variant_alone = []
            all_variants = [[]]
            #only if requested create variants
            if ((tmpl_id.no_create_variants == 'empty' and
                    not tmpl_id.categ_id.no_create_variants) or
                    tmpl_id.no_create_variants == 'no'):
                # list of values combination
                # START MODIFICATION
                attribute_line_with_child_ids = tmpl_id.attribute_line_ids.\
                    filtered("attribute_id.child_ids")
                attribute_line_ids = tmpl_id.attribute_line_ids - \
                                     attribute_line_with_child_ids
                if not attribute_line_with_child_ids:
                    for variant_id in tmpl_id.attribute_line_ids:
                        if len(variant_id.value_ids) == 1:
                            variant_alone.append(variant_id.value_ids[0])
                else:
                    for attribute_line_with_child_id in attribute_line_with_child_ids: # per ogni categoria
                        for variant_id in attribute_line_with_child_id.attribute_id.child_ids: # per ogni materiale ['mat','colore','imp']
                            temp_variants = []
                            # for variant in all_variants:
                            for value_id in variant_id.value_ids:#aggiungo pelle e tutti i colori della pelle
                                temp_variants.append(sorted([int(value_id)]))

                            if temp_variants:
                                all_variants += temp_variants
                temp_variants = []
                # aggiungo ad ogni variante pelle/colore l'impuntura (la sequence dell'impuntura dev'essere > 0)
                if attribute_line_ids:
                    for standard_variant_id in attribute_line_ids:
                        for value_id in standard_variant_id.value_ids:
                            for variant in all_variants:
                                temp_variants.append(sorted(variant + [int(value_id)]))

                if attribute_line_with_child_ids:
                    for variant in temp_variants:
                        if len(variant) < 2:
                            temp_variants.pop(temp_variants.index(variant))

                if temp_variants:
                    all_variants = temp_variants
                # END MODIFICATION

                # adding an attribute with only one value should not recreate product
                # write this attribute on every product to make sure we don't lose them
                for variant_id in variant_alone:
                    product_ids = []
                    for product_id in tmpl_id.product_variant_ids:
                        if variant_id.id not in map(int, product_id.attribute_value_ids):
                            product_ids.append(product_id.id)
                    product_obj.write(cr, uid, product_ids, {'attribute_value_ids': [(4, variant_id.id)]}, context=ctx)

                # check product
                variant_ids_to_active = []
                variants_active_ids = []
                variants_inactive = []
                for product_id in tmpl_id.product_variant_ids:
                    variants = sorted(map(int,product_id.attribute_value_ids))
                    if variants in all_variants:
                        variants_active_ids.append(product_id.id)
                        all_variants.pop(all_variants.index(variants))
                        if not product_id.active:
                            variant_ids_to_active.append(product_id.id)
                    else:
                        variants_inactive.append(product_id)
                if variant_ids_to_active:
                    product_obj.write(cr, uid, variant_ids_to_active, {'active': True}, context=ctx)

                # create new product
                for variant_ids in all_variants:
                    values = {
                        'product_tmpl_id': tmpl_id.id,
                        'attribute_value_ids': [(6, 0, variant_ids)]
                    }
                    id = product_obj.create(cr, uid, values, context=ctx)
                    variants_active_ids.append(id)

                # unlink or inactive product
                for variant_id in map(int,variants_inactive):
                    try:
                        with cr.savepoint(), tools.mute_logger('openerp.sql_db'):
                            product_obj.unlink(cr, uid, [variant_id], context=ctx)
                    except UserError(psycopg2.Error):
                        product_obj.write(cr, uid, [variant_id], {'active': False}, context=ctx)
                        pass
            else:
                if not tmpl_id.attribute_line_ids:
                    # create new product "alone"
                    for variant_ids in all_variants:
                        values = {
                            'product_tmpl_id': tmpl_id.id,
                            'attribute_value_ids': [(6, 0, variant_ids)]
                        }
                        product_obj.create(cr, uid, values, context=ctx)
        return True

    @api.multi
    def name_get(self):
        res = []
        for product_tmpl in self:
            name = "%s%s" % (
                product_tmpl.prefix_code and (
                    product_tmpl.prefix_code + u' - ') or '',
                product_tmpl.name,
            )
            res.append((product_tmpl.id, name))
        return res

    @api.multi
    def _get_product_attributes_dict(self):
        if not self:
            return []
        self.ensure_one()
        res = []
        for attribute_line in self.attribute_line_ids.sorted(
                lambda x: x.attribute_id.sequence):
            if attribute_line.attribute_id.child_ids:
                for child in attribute_line.attribute_id.child_ids:
                    res.append({'attribute_id': child.id})
            else:
                res.append({'attribute_id': attribute_line.attribute_id.id})
        return res
