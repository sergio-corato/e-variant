# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* product_variant_add_all_value_attribute
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 8.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-06-07 21:34+0000\n"
"PO-Revision-Date: 2017-06-07 21:34+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: product_variant_add_all_value_attribute
#: model:ir.actions.server,name:product_variant_add_all_value_attribute.action_product_attribute_add_all
msgid "Add attribute values Wizard"
msgstr "Aggiungi tutti gli attributi"

#. module: product_variant_add_all_value_attribute
#: model:ir.model,name:product_variant_add_all_value_attribute.model_product_template
msgid "Product Template"
msgstr "Template Prodotto"
