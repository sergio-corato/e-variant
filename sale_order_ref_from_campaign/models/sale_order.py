# -*- coding: utf-8 -*-

from openerp import models, api


class SaleOrder(models.Model):
    _inherit = "sale.order"

    @api.onchange('campaign_id')
    def onchange_campaign_id(self):
        self.client_order_ref = self.campaign_id.name
