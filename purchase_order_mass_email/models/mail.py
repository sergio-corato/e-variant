# -*- coding: utf-8 -*-
from openerp import models, api, exceptions, fields, _
from time import time
import base64
import openerp


class MailComposeMessage(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.model
    def generate_email_for_composer_batch(
            self, template_id, res_ids, context=None, fields=None):
        values = super(MailComposeMessage, self).generate_email_for_composer_batch(
            template_id, res_ids, context=context, fields=fields
        )
        if self._context.get('active_model', False) == 'purchase.order':
            report_name = 'purchase.report_purchaseorder_aeroo'
            template = 'email_template_purchase_mass'
            ir_actions_report = self.env['ir.actions.report.xml']
            report = ir_actions_report.search([
                ('report_name', '=', report_name)
            ], limit=1)
            template_id = self.env['ir.model.data'].get_object_reference(
                'purchase_order_mass_email', template
            )
            if not template_id:
                raise exceptions.ValidationError(_('No template found!'))
            if self._context.get('default_template_id', False) == template_id[1]:
                attachment_ids = []
                attachment_obj = self.env['ir.attachment']
                objects = self.env[self._context['active_model']].browse(
                    self._context['active_ids'])
                if any([x.exported_attachment_id for x in objects]):
                    raise exceptions.Warning(_(
                        'PO %s are already exported and sent to supplier!' % (
                            ' '.join([
                                x.name for x in objects if x.exported_attachment_id])
                        )
                    ))
                partners = objects.mapped('partner_id')
                if len(partners) > 1:
                    raise exceptions.Warning(_(
                        'More than 1 partner selected!'
                    ))
                partner = partners[0]
                if partner.mail_purchase_ir_exports_id:
                    wizard = self.env['export.wizard.ept']
                    config = wizard.default_get(list(wizard.fields_get()))
                    wiz_id = wizard.create(config)
                    res = wiz_id.with_context(
                        active_id=partner.mail_purchase_ir_exports_id.id,
                        export_domain="[('id', 'in', %s)]" % self._context['active_ids']
                    ).download_file()
                    if res.get('url', False):
                        export_id = eval(res['url'].split('id=')[1])
                        attachment_ids += [export_id]
                for obj in objects:
                    if report:
                        (result, format) = openerp.report.render_report(
                            self._cr, self._uid, [obj.id],
                            report.report_name,
                            {'model': obj._name},
                            self._context)
                        eval_context = {'time': time, 'object': obj}
                        if not report.attachment or not eval(
                                report.attachment, eval_context):
                            # no auto-saving of report as attachment,
                            # need to do it manually
                            result = base64.b64encode(result)
                            file_name = '%s_%s.pdf' % (
                                obj.partner_id.name,
                                obj.name)
                            attachment_id = attachment_obj.create({
                                    'name': file_name,
                                    'datas': result,
                                    'datas_fname': file_name,
                                    'res_model': obj._name,
                                    'res_id': obj.id,
                                    'type': 'binary'
                                })
                            attachment_ids += [attachment_id.id]
                values[values.keys()[0]].update({
                    'attachment_ids': attachment_ids,
                    'composition_mode': 'comment',
                })
        return values

    @api.multi
    def send_mail(self):
        if self._context.get('active_model') == 'purchase.order'\
                and self._context.get('default_active_ids')\
                and self._context.get('default_mark_po_as_exported'):
            self.env['purchase.order'].browse(
                self._context['default_active_ids']).write({
                    'sent': True,
                    'date_sent': fields.Datetime.now(),
                })
        return super(MailComposeMessage, self).send_mail()
