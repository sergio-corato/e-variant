# -*- coding: utf-8 -*-
from openerp import fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    mail_purchase_ir_exports_id = fields.Many2one(
        'ir.exports', string='Export to be sent with mail of purchase order'
    )
