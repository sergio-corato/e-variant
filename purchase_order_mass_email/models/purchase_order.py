# -*- coding: utf-8 -*-
from openerp import fields, models


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    exported_attachment_id = fields.Many2one(
        'ir.attachment',
        string='File Exported sent by mail',
        readonly=True
    )
    sent = fields.Boolean()
    date_sent = fields.Datetime()
