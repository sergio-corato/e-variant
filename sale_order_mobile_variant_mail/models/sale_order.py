# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, api, exceptions


class SaleOrder(models.Model):
    _inherit = "sale.order"

    @api.multi
    def action_mobile_quotation_send(self):
        #Create action from standard function to use modified template
        action_dict = super(SaleOrder, self).action_quotation_send()
        try:
            template_id = self.env['ir.model.data'].get_object_reference(
                'sale_order_mobile_variant_mail',
                'email_sale_order_mobile_variant')[1]
            # assume context is still a dict, as prepared by super
            ctx = action_dict['context']
            ctx['default_template_id'] = template_id
            ctx['default_use_template'] = True
        except exceptions:
            pass
        return action_dict
