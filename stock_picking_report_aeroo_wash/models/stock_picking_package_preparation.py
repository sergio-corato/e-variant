# -*- coding: utf-8 -*-

from openerp import models, api, fields


class StockPickingPackagePreparation(models.Model):
    _inherit = 'stock.picking.package.preparation'

    @api.multi
    def _get_eur1(self):
        for pick in self:
            if pick.partner_shipping_id.country_id in \
                    pick.company_id.eur1_country_group_id.country_ids:
                pick.is_eur1 = True

    @api.multi
    def _get_tsca(self):
        for pick in self:
            for pt in pick.line_ids.mapped('product_id.product_tmpl_id'):
                if pt.filtered(lambda x: bool(
                        [y for y in x.customs_info_ids if y.print_tsca])):
                    pick.is_tsca = True
                    break

    @api.multi
    def _get_lacey(self):
        for pick in self:
            for pt in pick.line_ids.mapped('product_id.product_tmpl_id'):
                if pt.filtered(lambda x: bool(
                        [y for y in x.customs_info_ids if y.print_lacey])):
                    pick.is_lacey = True
                    break

    is_eur1 = fields.Boolean(compute=_get_eur1)
    is_tsca = fields.Boolean(compute=_get_tsca)
    is_lacey = fields.Boolean(compute=_get_lacey)

    @api.multi
    def print_washington_declaration(self):
        return self.env['report'].get_action(
            self, 'stock.report_picking_wash')

    @api.multi
    def print_eur1_declaration(self):
        return self.env['report'].get_action(
            self, 'stock.report_picking_eur1')

    @api.multi
    def print_tsca_declaration(self):
        return self.env['report'].get_action(
            self, 'stock.report_picking_tsca')

    @api.multi
    def print_lacey_declaration(self):
        return self.env['report'].get_action(
            self, 'stock.report_picking_lacey')
