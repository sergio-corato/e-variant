# -*- coding: utf-8 -*-
# Copyright 2019 Sergio Corato <https://github.com/sergiocorato>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

from openerp import fields, models


class ResCompany(models.Model):
    _inherit = 'res.company'

    eur1_country_group_id = fields.Many2one(
        'res.country.group', string='EUR1 Country group')
